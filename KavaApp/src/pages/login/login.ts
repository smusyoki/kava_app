import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Nav, AlertController, ToastController, LoadingController, App } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { FormGroup, FormControl, Validators, FormBuilder, AbstractControl } from '@angular/forms';


@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  public token: string;
  public loader: any;

  loginForm: FormGroup;
  email: AbstractControl;
  password: AbstractControl;

  constructor( public navCtrl: NavController, public navParams: NavParams, public api: ApiProvider, private alertCtrl: AlertController,
               private toastCtrl: ToastController, public loadingCtrl: LoadingController, public formBuilder: FormBuilder,
                public appCtrl: App ) {


                this.loginForm = formBuilder.group({
                  email: ['', [Validators.required, Validators.email]],
                  password: ['', Validators.required]
                });

                this.email = this.loginForm.controls["email"];
                this.password = this.loginForm.controls["password"];

                //console.log("Username: ", this.username);
                //console.log("Password: ", this.password);

  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad LoginPage');
  }

  login(){

    if( this.loginForm.valid ){

      this.showLoading();

      let user_details = {
        'email': this.email.value.toLowerCase().trim(),
        'password': this.password.value.trim()
      }

      this.api.login(user_details).subscribe( (data: any) => {

        console.log("Login: ", data);

        this.loader.dismiss();

        if ( data.access_token ){

            this.navCtrl.setRoot('HomePage', {post: data});
        }

        if (data.message == "Invalid credentials"){
          this.showToast("Invalid credentials");
        }

      }, error => {

        this.loader.dismiss();
        this.showToast("Error while login in. Please try again.");

        console.log("Registration error: ", error);

      });



    }else{
      this.showToast("Errors detected. Please check the input again.");
    }

  }

  register(){
    this.loginForm.markAsUntouched(); //Reset any error messages
    this.navCtrl.push('RegisterPage');
  }

  showAlert(title: string = "Message", message: string = "Message" ) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: ['OK']
    });
    alert.present();
  }

  showToast(message: string = "Message" ) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 5000,
      position: 'bottom',
      dismissOnPageChange: true,
      closeButtonText: "X",
      showCloseButton: false
    });

    toast.present();
  }

  showLoading() {
    this.loader = this.loadingCtrl.create({
      content: "Login in...",
      duration: 20000
    });

    this.loader.present();

  }

}
