import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ToastController, LoadingController } from 'ionic-angular';
import { FormGroup, FormControl, Validators, FormBuilder, AbstractControl } from '@angular/forms';
import { PasswordValidator } from '../../shared/password.validator';
import { ApiProvider } from '../../providers/api/api';

import { LoginPage } from '../login/login';


@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  public token: string;
  public loader: any;
  public userURL = '';

  registerForm: FormGroup;
  name: AbstractControl;
  email: AbstractControl;
  password: AbstractControl;
  confirmPassword: AbstractControl;

  constructor(public navCtrl: NavController, public navParams: NavParams, public api: ApiProvider, private alertCtrl: AlertController,
    private toastCtrl: ToastController, public loadingCtrl: LoadingController, public formBuilder: FormBuilder) {

      this.registerForm = this.formBuilder.group({
        name: ['', [Validators.required, Validators.minLength(3)]],
        email: ['', [Validators.required, Validators.email]],
        password: ['', [Validators.required, Validators.minLength(6)]],
        confirmPassword: ['', Validators.required]
      }, {validator: PasswordValidator});

      this.name = this.registerForm.controls["name"];
      this.email = this.registerForm.controls["email"];
      this.password = this.registerForm.controls["password"];
      this.confirmPassword = this.registerForm.controls["confirmPassword"];

  }


  ionViewDidLoad() {
    //console.log('ionViewDidLoad RegisterPage');
  }


  register(){

    if ( this.registerForm.valid ){

      this.showLoading();

       //console.log(this.registerForm.value);

      let user_details = {
        'name': this.name.value.toUpperCase().trim(),
        'email': this.email.value.toLowerCase().trim(),
        'password': this.password.value.trim()
      }

      this.api.register(user_details).subscribe( (data: any) =>{

        console.log("Register: ", data);

        this.loader.dismiss();

        if ( data.access_token ){
            this.registerForm.reset();
            this.showAlert("Account Signup","Your account has been created.");
        }



      }, error => {

        this.loader.dismiss();
        this.showToast("Error while creating account. Please try again.");

        console.log("Registration error: ", error);

      });



    }else{
      this.showToast("Errors detected. Please check the input again.");
    }

  }


  showAlert(title: string = "Message", message: string = "Register Message" ) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: [{
        text: 'OK',
        handler: () => {
          console.log('OK clicked');
          //this.navCtrl.pop();
          this.navCtrl.setRoot('LoginPage');
        }

      }]
    });
    alert.present();
  }

  showToast(message: string = "Register Message" ) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 5000,
      position: 'bottom',
      dismissOnPageChange: true,
      closeButtonText: "X",
      showCloseButton: false
    });

    toast.present();
  }

  showLoading() {
    this.loader = this.loadingCtrl.create({
      content: "Creating account...",
      duration: 20000
    });

    this.loader.present();

  }

}
