import { Component } from '@angular/core';
import { ApiProvider } from '../../providers/api/api';
import { IonicPage, NavController, NavParams, LoadingController, AlertController, ToastController } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  public loader: any;
  public user_data: any;
  public expenses: any = [];
  public total: number = 0;
  private isLoading: boolean = true;

  constructor(public navCtrl: NavController, public navParams: NavParams, public api: ApiProvider, public loadingCtrl: LoadingController,
              private alertCtrl: AlertController, private toastCtrl: ToastController) {

                this.user_data = navParams.get("post");

                console.log(this.user_data);

                this.getExpenses();

  }

  getExpenses(){

    this.api.getExpenses(this.user_data.user.id, this.user_data.access_token).subscribe( (data: any) => {

      console.log("User Expenses:  ", data);

      this.expenses = data;
      this.isLoading = false;

      //Get total expenses
      this.total = 0;
      for (let n of this.expenses){
        this.total += parseFloat(n.amount)
      }


    }, error => {
      console.log("Expenses error: ", error);
      this.isLoading = false;

      if (error.error.message == "Unauthenticated." ){
        this.showToast("Session expired.");
      }else{
        this.showToast("Could not get expenses.");
      }

    });

  }

  addExpense(){

    let alert = this.alertCtrl.create({

    title: 'Add an expense',
    enableBackdropDismiss: false,
    inputs: [
      {
        name: 'item',
        placeholder: 'Item'
      },
      {
        name: 'amount',
        placeholder: 'Amount',
        type: 'number'
      },
      {
        name: 'user_id',
        type: 'hidden',
        value: this.user_data.user.id
      }
    ],
    buttons: [
      {
        text: 'Cancel',
        role: 'cancel',
        handler: data => {
          //console.log('Expense Cancel clicked');
        }
      },
      {
        text: 'Add Expense',
        handler: expenseData => {

          if ( expenseData.item == '' || expenseData.item == null || expenseData.amount == '' || expenseData.amount == null ){
            this.showToast("All fields are required");
            return false;
          }

          if ( isNaN( parseFloat(expenseData.amount) ) ){
            this.showToast("Amount is invalid");
            return false;
          }

          expenseData.amount.replace(',', ''); //remove commas


          this.showLoading("Adding Expense...");

          console.log("Expense data: ", expenseData);

          this.api.addExpenses(expenseData, this.user_data.access_token).subscribe( (data: any) => {

            this.loader.dismiss();
            this.showToast("Expense added.");

            this.getExpenses();

            console.log("Expense added: ", data);

          }, error =>{

            this.loader.dismiss();


            console.log("Expense add error: ", error);

            if (error.error.message == "Unauthenticated." ){
              this.showToast("Session expired.");
            }else{
              this.showToast("Error while adding expense. Please try again.");
            }

            return false;

          });



        }
      }
    ]
  });
  alert.present();

}

showToast(message: string = "Expense Message" ) {
  let toast = this.toastCtrl.create({
    message: message,
    duration: 5000,
    position: 'bottom',
    dismissOnPageChange: true,
    closeButtonText: "X",
    showCloseButton: true
  });

  toast.present();
}

showLoading(message: any = "Working...") {
  this.loader = this.loadingCtrl.create({
    content: message,
    duration: 20000
  });

  this.loader.present();

}


}
