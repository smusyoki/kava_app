import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';


@Injectable()
export class ApiProvider {

  private URL:string = "http://6af6cdf1.ngrok.io/api";
  public expenses:any = [];

  constructor(public http: HttpClient) {

  }


  register(data){
    let headers = new HttpHeaders();
    headers = headers.set('Accept', 'application/json');

    return  this.http.post(this.URL + '/register', data, {headers: headers});
  }

  login(data){
    let headers = new HttpHeaders();
    headers = headers.set('Accept', 'application/json');

    return  this.http.post(this.URL + '/login', data, {headers: headers});

  }

  addExpenses( data , token: any){
    let headers = new HttpHeaders();
    headers = headers.set('Accept', 'application/json');
    headers = headers.append('Authorization', 'Bearer ' + token);


    return this.http.post(this.URL + '/expenses', data, {headers: headers});
 }

  getExpenses(id: any, token: any){

    let headers = new HttpHeaders();
    headers = headers.set('Accept', 'application/json')
                      .set('Authorization', 'Bearer ' + token);

    return this.http.get(this.URL + '/expenses/' + id,  {headers: headers});
  }



}
