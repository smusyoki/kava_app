<h1>Kava Expense Manager</h1>

<b>Manage your daily expenses.</b>

<p>Languages used; <br>
Back end - Laravel 6.2<br>
Hybrid Mobile App - Ionic 3
</p>

<b>Steps to view Laravel backend</b>
1. Install Laravel as per the Laravel documentation
2. Create a database
3. Configure database options in the .env file
4. Open command prompt and navigate to the laravel directory
5. Run -- composer install -- to install the project dependencies from the composer.lock or composer.lock file,
6. Run -- php artisan migrate -- to migrate the tables
7. Run command -- php artisan serve -- to launch php serve at port 8000

<b>Steps to view the Ionic 3 mobile</b>
1. Install Ionic as per the ionic documentation
2. Go to KavaApp -> src -> Providers -> api -> api.ts
3. Change URL to your host url -> https://www.your_url.com/api
4. Open command prompt and navigate to KavaApp directory
5. Run command -- npm install --
6. Run command -- ionic serve -- to launch the app on the browser
