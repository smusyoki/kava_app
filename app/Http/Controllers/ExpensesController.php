<?php

namespace App\Http\Controllers;

use App\User;
use App\Expense;
use Illuminate\Http\Request;

class ExpensesController extends Controller
{

    public function register(Request $request){

        $data = request()->validate([
                'name' => 'required|min:3',
                'email' => 'required|email|unique:users',
                'password' => 'required'
            ]);

            $data['password'] = bcrypt($request->password); //encrypt password

            $user = User::create($data);

            $accessToken = $user->createToken('authToken')->accessToken;

            return response( ['user' => $user, 'access_token' => $accessToken] );

    }

    public function login(Request $request){

        $data = request()->validate([
            'email' => 'required|email',
            'password' => 'required'
        ]);

        if (!auth()->attempt($data)) {
            return response(['message' => 'Invalid credentials']);
        }

        $accessToken = auth()->user()->createToken('authToken')->accessToken;

        return response(['user' => auth()->user(), 'access_token' => $accessToken]);

    }

    public function index(){

        $expenses = Expense::all();

        return $expenses;

    }

    public function store(Request $request){

        $data = request()->validate([
            'user_id' => 'required',
            'item' => 'required',
            'amount' => 'required|numeric'
        ]);

        $expense = Expense::create($data);
        return $expense;
    }


    public function show($user){

        $expenses = Expense::where('user_id', $user)
                    ->orderBy('id', 'desc')
                    ->get();

        return $expenses;

    }

}
